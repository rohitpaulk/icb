function activatedropdown() {
    $('#header-dropdown-wrapper').stop(true,true).slideDown('medium');    
}
function deactivatedropdown() {    
    $('#header-dropdown-wrapper').stop(true,true).slideUp('medium');    
}
function toggledropdown() {
    if($('#header-dropdown-wrapper').is(":visible")){
        deactivatedropdown();
    }
    else {
        activatedropdown();
    }
}

function hidemenu(){
    $('.header-bar-container #completemenu').hide();        
}
function showmenu() {
    
    $('.header-bar-container #completemenu').stop(true,true).slideDown('medium');    
}

function togglemenu() {
    console.log($('.header-bar-container #completemenu').height());
    if($('.header-bar-container #completemenu').is(":visible")){
    //if($('.header-bar-container #completemenu').height() > 200){
        hidemenu();
    }
    else{
        showmenu();
    }
}

// Function to deactivate dropdown only if the mouse doesn't go into dropdown-wrapper
function positionaldeactivate() {
    if (!$('#header-dropdown-wrapper').is(':hover')) {
        deactivatedropdown();
    }
}

function WidthChange(mq){
    if(mq.matches){ // > 480px
        $('.header-bar-container #activate-dropdown').on('mouseleave',positionaldeactivate);
        $('.header-bar-container #activate-dropdown').on('mouseenter',activatedropdown);
        showmenu();
    }
    else{ // < 480px
        $('.header-bar-container #activate-dropdown').off('mouseleave',positionaldeactivate);        
        $('.header-bar-container #activate-dropdown').off('mouseenter',activatedropdown);                
        hidemenu(); // MEDIA QUERUES!!!!
    }
}




function activatetab(panelid) {
    // CAR
    if (panelid == 1){ 
        
        $('#car-dropdown-container').fadeIn(400);
        $('#bikes-dropdown-container').hide();
    }
    // BIKE
    if (panelid == 2){
        $('#bikes-dropdown-container').fadeIn(400);
        $('#car-dropdown-container').hide();
    }
}


$(document).ready(function () {
    
    // Deactivation is dependent on Media Query
    var mq = window.matchMedia("(min-width: 480px)");
    mq.addListener(WidthChange);
    WidthChange(mq);
    $('#header-dropdown-wrapper').on('mouseleave',deactivatedropdown);
    $('#activate-car-dropdown').on('mouseenter',function(){
        activatetab(1);
    });
    $('#activate-car-dropdown,#activate-bikes-dropdown ').on('tap',toggledropdown);
    $('#activate-bikes-dropdown').on('mouseenter',function(){
        activatetab(2);
    });
    $('.header-bar-container #expandmenu').on('click',togglemenu);
    window.onscroll = function(){
        if($(document).scrollTop() > 120){
            $('.header-bar-container').addClass('shrunk');
            $('.header-ticker-wrapper').addClass('shrunk');
            //$('.header-ticker-wrapper').hide();    
        }
        else{
            $('.header-bar-container').removeClass('shrunk');
            $('.header-ticker-wrapper').removeClass('shrunk');
            //$('.header-ticker-wrapper').show(); 
        }
    }
    
});